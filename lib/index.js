'use strict'

// The name of the package in order to give the Antora logger a useful name
const { name: packageName } = require('../package.json')

var logger
var linecounter = 0

module.exports.register = function ({ config }) {
    logger = this.require('@antora/logger').get(packageName)
    logger.info("registered")

    this.on('contentClassified', function ({ contentCatalog }) {
        const sym = Object.getOwnPropertySymbols(contentCatalog).find(function (s) {
            return String(s) === "Symbol(files)";
        });

        //var i = 0;

        for (const [key, value] of contentCatalog[sym].get('page')) {
            logger.info(key+" : "+value)
            for (const source of config.data.src) {
                logger.info(source.component)
                if (key.includes(source.component)) {
                    if (!source.forbidden.some(v => key.includes(v))) {
                        logger.debug("looking at key " + key);
                        /*if (i < 3) {
                            i++;
                            console.log("value");
                            console.log(value);
                            console.log("content:");
                            console.log(value._contents.toString());
                        }*/
                        value._contents = Buffer.from(format(value._contents.toString(), source.default_enable), "utf-8");
                    }
                }
            }
        }
        logger.info("altered " + linecounter + " lines")
    })
}

function format(data, default_enable) {
    var lines = data.split(/\r?\n/);

    logger.debug("file lines: " + lines.length.toString());

    var doc_part;

    lines.forEach(str => {
        //logger.debug("line length: " + str.length.toString())
        if (str.startsWith(':doc-part:')) {
            doc_part = str.substring(11);
        }
    });

    //throw new Error();

    //const docpartindex = data.indexOf(':doc-part:')
    //const doc_part = data.substring(data.indexOf(':', docpartindex + 1) + 2, data.indexOf('\r', docpartindex))


    var enablenum = false
    var current_lvl = [doc_part, 1, 1, 1, 1, 1]

    var last_level = 0

    for (let index = 0; index < lines.length; index++) {
        var line = lines[index]

        /*if(line.length < 20) {
            logger.debug("line: '" + line + "'");
        }*/

        enablenum = setnum(line, enablenum)

        var level = -1
        if (line.indexOf("=") == 0) {
            level = line.indexOf(" ") - 1;
            for (let i = 0; i <= level; i++) {
                if (line.charAt(i) != "=") {
                    logger.debug("skipping line: '" + line + "' because of char: '" + line.charAt(i) + "' at " + i);
                    level = -1;
                }
            }
        }

        if (level >= 0 && (enablenum || default_enable)) {
            if (level <= last_level && level != 0) {
                current_lvl[level] = current_lvl[level] + 1

            }
            if (level < last_level && level != 0) {
                for (let i = level + 1; i < current_lvl.length; i++) {
                    current_lvl[i] = 1
                }
            }
            last_level = level

            var part = ""
            for (let i = 0; i <= level; i++) {
                // prefix Appendix and postfix with ':' if level = 0 and first char level is uppercase
                if (level==0 && /[A-Z]/.test( current_lvl[i][0])) {
                    part += "Appendix " + current_lvl[i] + ":"
                } else {
                    // Add . only at top-level and inbetween levels
                    part += current_lvl[i] + (i!=0 && i==level ? "" : ".")
                }
            }

            line = line.substring(0, line.indexOf(' ')) + " " + part + " " + line.substring(line.indexOf(' ') + 1);
            logger.debug("new line (" + index + "): " + line);
            lines[index] = line;
            linecounter++;
        }
    }
    var out = "";
    for (let index = 0; index < lines.length; index++)
        out += lines[index] + '\r\n';

    return out
}

function setnum(element, enablenum) {
    if (element.indexOf(':numbering:') == 0) {
        logger.debug("enablenum")
        enablenum = true
    }
    if (element.indexOf(':!numbering:') == 0) {
        logger.debug("disablenum")
        enablenum = false
    }
    return enablenum
}
